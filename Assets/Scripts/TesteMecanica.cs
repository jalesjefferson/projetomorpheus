﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TesteMecanica : MonoBehaviour
{

    public GameObject mira, player;
    public float veloMira;
    bool miraAtiva;
    public LayerMask hitMask;
    public RaycastHit2D hit;
    

    private void Start()
    {
        if (mira.activeSelf)
            miraAtiva = true;
    }

    void Update()
    {
        LigarMirar();
        MoverMira();
        AnalizarDirecao();
    }

    void LigarMirar()
    {
        if (Input.GetButtonDown("Jump"))
        {
            mira.SetActive(!mira.activeSelf);
            miraAtiva = mira.activeSelf;
        }
    }

    void MoverMira()
    {
        if (miraAtiva)
        {
            var x = Input.GetAxisRaw("Horizontal");
            var y = Input.GetAxisRaw("Vertical");
            x = x * Time.deltaTime * veloMira;
            y = y * Time.deltaTime * veloMira;
            mira.transform.Translate(x, y, 0);
        }
    }

    void AnalizarDirecao()
    {
        if (Input.GetButtonDown("Fire1") && miraAtiva)
        {
            var dis = Vector2.Distance(player.transform.position, mira.transform.position);
            Debug.DrawRay(player.transform.position, mira.transform.position - player.transform.position, Color.green,1);
            hit = Physics2D.Raycast(player.transform.position, mira.transform.position - player.transform.position, dis, hitMask);
            if (hit)
            {
                if (AnalizarPosicao())
                    Debug.Log("Avistei um: " + hit.transform.gameObject.name);
                else if (hit.transform.gameObject.CompareTag("Parede"))
                    Debug.Log("Tem uma parade na frente da minha mira investigativa");
                else
                    Debug.Log("Algo nessa direcao");
            }

            else
                Debug.Log("Nada nessa direção");
        }
    }

    bool AnalizarPosicao()
    {
        var top = Physics2D.OverlapCircle(mira.transform.position, 0.2f);

        if (top && top.transform.gameObject.name == hit.transform.gameObject.name)
            return true;
        else
            return false;
    }
}