﻿using UnityEngine;

public class Player : Pessoa
{
    void Awake()
    {
               
    }

    void Update()
    {
        circlePos = gameObject.transform.position - new Vector3(0, transform.localScale.y / 2,0);
        CheckGroud();
        CheckInput();
    }

    void CheckInput()
    {
        if(Input.GetAxisRaw("Horizontal") != 0)
        {
            float x = Input.GetAxisRaw("Horizontal");
            Walk(walkSpeed, x);
        }
    }
}
