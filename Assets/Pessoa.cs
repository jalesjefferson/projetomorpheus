﻿using UnityEngine;

public class Pessoa : MonoBehaviour
{
    public int health;
    public float walkSpeed;
    public bool onGroud = false;
    public Vector2 circlePos;
    public LayerMask chao;

    public void CheckGroud(){
        if (!Physics2D.OverlapCircle(circlePos, 0.1f, chao)){
            Gravity();
        }
    }

    public void Gravity()
    {
        float fall = -2 * Time.deltaTime;
        gameObject.transform.Translate(0, fall, 0);
    }

    public void Walk(float speed, float dir)
    {
        Mathf.RoundToInt(dir);
        gameObject.transform.Translate(speed * dir * Time.deltaTime, 0, 0);
    }
}


